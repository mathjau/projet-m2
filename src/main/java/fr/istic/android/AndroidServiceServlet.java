package fr.istic.android;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.googlecode.jsonrpc4j.JsonRpcServer;

public class AndroidServiceServlet extends HttpServlet {
	
        private static final long serialVersionUID = 7761537674133167249L;
        private IAPPService myService;
        private JsonRpcServer jsonRpcServer;
        
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        		try {
                        jsonRpcServer.handle(req, resp);
                } catch (IOException e) {
                        e.printStackTrace();
                }
        }
        public void init(ServletConfig config) {
        	//Class d'implementation de votre service
        	this.myService= new AAPServiceImpl();
        	this.jsonRpcServer = new JsonRpcServer(this.myService, IAPPService.class);
        }
}