package fr.istic.android;

import com.googlecode.jsonrpc4j.JsonRpcService;

@JsonRpcService("IAPPService")
public interface IAPPService {
	boolean isCorrect(String message);
}
