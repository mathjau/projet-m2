package fr.istic.jna;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.util.HashMap;

import org.gwtrpc4j.RpcServiceProxyFactory;
import org.gwtrpc4j.http.jse.JSERequestBuilderFactory;

import com.google.gwt.user.client.rpc.SerializationException;
import com.sun.jna.Library;
import com.sun.jna.Native;

import fr.istic.client.GreetingService;

public class TestJna {

	public interface ITestJNA extends Library {
        String getCpuUsage();
        ITestJNA INSTANCE = (ITestJNA) Native.loadLibrary("/Users/aaa/Desktop/M2/Obligatoires/GLA/tp5s/libtestJNA.so.1.0",
                        ITestJNA.class, new HashMap<Object, Object>());
	}
	
	public static void main(String[] args) throws InterruptedException, SerializationException {
		new TestJna().testSyncro();
	}
	
	String moduleBaseURL = "http://localhost:8888/firstgwt/";
	String serializationPolicyStrongName = "B6606E16EECCC55FF3C5DF9BA8992CB9";
	String remoteServiceRelativePath = "greet";
	
	public void testSyncro() throws InterruptedException {
		JSERequestBuilderFactory factory = new JSERequestBuilderFactory();
		factory.setTimeoutMillis(10000);
		// disable proxy
		factory.setProxy(new Proxy(Type.HTTP, InetSocketAddress.createUnresolved("localhost", 8888)));
		final GreetingService gs = RpcServiceProxyFactory.create(GreetingService.class, moduleBaseURL, remoteServiceRelativePath, serializationPolicyStrongName,factory);
		System.setProperty("jna.library.path","/Users/aaa/Desktop/M2/Obligatoires/GLA/tp5s");
        String v = ITestJNA.INSTANCE.getCpuUsage();
        gs.pushConso(v);
		String firstTime = gs.greetServer("Nicolas  " + v);
		System.err.println(firstTime);
	}
}
