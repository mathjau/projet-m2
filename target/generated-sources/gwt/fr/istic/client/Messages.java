package fr.istic.client;

/**
 * Interface to represent the messages contained in resource bundle:
 * 	/Users/aaa/Desktop/M2/Obligatoires/GLA/TP3/TPGWT/projet-m2/src/main/resources/fr/istic/client/Messages.properties'.
 */
public interface Messages extends com.google.gwt.i18n.client.Messages {
  
  /**
   * Translated "Afficher les utilisateurs".
   * 
   * @return translated "Afficher les utilisateurs"
   */
  @DefaultMessage("Afficher les utilisateurs")
  @Key("btnAfficherLesUtilisateurs_html")
  String btnAfficherLesUtilisateurs_html();

  /**
   * Translated "Supprimer".
   * 
   * @return translated "Supprimer"
   */
  @DefaultMessage("Supprimer")
  @Key("btnSupprimer_html")
  String btnSupprimer_html();

  /**
   * Translated "Supprimer".
   * 
   * @return translated "Supprimer"
   */
  @DefaultMessage("Supprimer")
  @Key("btnSupprimer_html_1")
  String btnSupprimer_html_1();

  /**
   * Translated "Supprimer".
   * 
   * @return translated "Supprimer"
   */
  @DefaultMessage("Supprimer")
  @Key("btnSupprimer_html_2")
  String btnSupprimer_html_2();

  /**
   * Translated "Supprimer".
   * 
   * @return translated "Supprimer"
   */
  @DefaultMessage("Supprimer")
  @Key("btnSupprimer_html_3")
  String btnSupprimer_html_3();

  /**
   * Translated "Créer un utilisateur".
   * 
   * @return translated "Créer un utilisateur"
   */
  @DefaultMessage("Créer un utilisateur")
  @Key("createPerson_html")
  String createPerson_html();

  /**
   * Translated "Nom".
   * 
   * @return translated "Nom"
   */
  @DefaultMessage("Nom")
  @Key("lblNom_text")
  String lblNom_text();

  /**
   * Translated "Prénom".
   * 
   * @return translated "Prénom"
   */
  @DefaultMessage("Prénom")
  @Key("lblPrnom_text")
  String lblPrnom_text();

  /**
   * Translated "cr".
   * 
   * @return translated "cr"
   */
  @DefaultMessage("cr")
  @Key("mntmCr_text")
  String mntmCr_text();

  /**
   * Translated "Enter your name".
   * 
   * @return translated "Enter your name"
   */
  @DefaultMessage("Enter your name")
  @Key("nameField")
  String nameField();

  /**
   * Translated "Send".
   * 
   * @return translated "Send"
   */
  @DefaultMessage("Send")
  @Key("sendButton")
  String sendButton();

  /**
   * Translated "Enter your First Name".
   * 
   * @return translated "Enter your First Name"
   */
  @DefaultMessage("Enter your First Name")
  @Key("txtbxEnterYourFirst_text")
  String txtbxEnterYourFirst_text();

  /**
   * Translated "Enter your Firstname".
   * 
   * @return translated "Enter your Firstname"
   */
  @DefaultMessage("Enter your Firstname")
  @Key("txtbxEnterYourFirstname_text")
  String txtbxEnterYourFirstname_text();
}
