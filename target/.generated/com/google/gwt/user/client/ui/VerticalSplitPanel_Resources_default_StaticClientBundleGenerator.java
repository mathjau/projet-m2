package com.google.gwt.user.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class VerticalSplitPanel_Resources_default_StaticClientBundleGenerator implements com.google.gwt.user.client.ui.VerticalSplitPanel.Resources {
  private static VerticalSplitPanel_Resources_default_StaticClientBundleGenerator _instance0 = new VerticalSplitPanel_Resources_default_StaticClientBundleGenerator();
  private void verticalSplitPanelThumbInitializer() {
    verticalSplitPanelThumb = new com.google.gwt.resources.client.impl.ImageResourcePrototype(
      "verticalSplitPanelThumb",
      com.google.gwt.safehtml.shared.UriUtils.fromTrustedString(bundledImage_None),
      0, 0, 7, 7, false, false
    );
  }
  private static class verticalSplitPanelThumbInitializer {
    static {
      _instance0.verticalSplitPanelThumbInitializer();
    }
    static com.google.gwt.resources.client.ImageResource get() {
      return verticalSplitPanelThumb;
    }
  }
  public com.google.gwt.resources.client.ImageResource verticalSplitPanelThumb() {
    return verticalSplitPanelThumbInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static final java.lang.String bundledImage_None = GWT.getModuleBaseURL() + "8603379B5088782D2C0620FAE856E112.cache.png";
  private static com.google.gwt.resources.client.ImageResource verticalSplitPanelThumb;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      verticalSplitPanelThumb(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("verticalSplitPanelThumb", verticalSplitPanelThumb());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'verticalSplitPanelThumb': return this.@com.google.gwt.user.client.ui.VerticalSplitPanel.Resources::verticalSplitPanelThumb()();
    }
    return null;
  }-*/;
}
