package fr.istic.client;

public class Messages_ implements fr.istic.client.Messages {
  
  public java.lang.String btnAfficherLesUtilisateurs_html() {
    return "Afficher les utilisateurs";
  }
  
  public java.lang.String btnSupprimer_html_1() {
    return "Supprimer";
  }
  
  public java.lang.String sendButton() {
    return "Send";
  }
  
  public java.lang.String mntmCr_text() {
    return "cr";
  }
  
  public java.lang.String btnSupprimer_html() {
    return "Supprimer";
  }
  
  public java.lang.String txtbxEnterYourFirst_text() {
    return "Enter your First Name";
  }
  
  public java.lang.String nameField() {
    return "Enter your name";
  }
  
  public java.lang.String createPerson_html() {
    return "Créer un utilisateur";
  }
  
  public java.lang.String txtbxEnterYourFirstname_text() {
    return "Enter your Firstname";
  }
  
  public java.lang.String btnSupprimer_html_2() {
    return "Supprimer";
  }
  
  public java.lang.String btnSupprimer_html_3() {
    return "Supprimer";
  }
  
  public java.lang.String lblNom_text() {
    return "Nom";
  }
  
  public java.lang.String lblPrnom_text() {
    return "Prénom";
  }
}
