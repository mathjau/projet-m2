package fr.istic.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Person_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.List getEd(fr.istic.shared.Person instance) /*-{
    return instance.@fr.istic.shared.Person::ed;
  }-*/;
  
  private static native void setEd(fr.istic.shared.Person instance, java.util.List value) 
  /*-{
    instance.@fr.istic.shared.Person::ed = value;
  }-*/;
  
  private static native java.util.List getFriends(fr.istic.shared.Person instance) /*-{
    return instance.@fr.istic.shared.Person::friends;
  }-*/;
  
  private static native void setFriends(fr.istic.shared.Person instance, java.util.List value) 
  /*-{
    instance.@fr.istic.shared.Person::friends = value;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native long getId(fr.istic.shared.Person instance) /*-{
    return instance.@fr.istic.shared.Person::id;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native void setId(fr.istic.shared.Person instance, long value) 
  /*-{
    instance.@fr.istic.shared.Person::id = value;
  }-*/;
  
  private static native java.util.List getMaison(fr.istic.shared.Person instance) /*-{
    return instance.@fr.istic.shared.Person::maison;
  }-*/;
  
  private static native void setMaison(fr.istic.shared.Person instance, java.util.List value) 
  /*-{
    instance.@fr.istic.shared.Person::maison = value;
  }-*/;
  
  private static native java.lang.String getName(fr.istic.shared.Person instance) /*-{
    return instance.@fr.istic.shared.Person::name;
  }-*/;
  
  private static native void setName(fr.istic.shared.Person instance, java.lang.String value) 
  /*-{
    instance.@fr.istic.shared.Person::name = value;
  }-*/;
  
  private static native java.lang.String getPrenom(fr.istic.shared.Person instance) /*-{
    return instance.@fr.istic.shared.Person::prenom;
  }-*/;
  
  private static native void setPrenom(fr.istic.shared.Person instance, java.lang.String value) 
  /*-{
    instance.@fr.istic.shared.Person::prenom = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, fr.istic.shared.Person instance) throws SerializationException {
    com.google.gwt.core.client.impl.WeakMapping.set(instance, "server-enhanced-data-1", streamReader.readString());
    setEd(instance, (java.util.List) streamReader.readObject());
    setFriends(instance, (java.util.List) streamReader.readObject());
    setId(instance, streamReader.readLong());
    setMaison(instance, (java.util.List) streamReader.readObject());
    setName(instance, streamReader.readString());
    setPrenom(instance, streamReader.readString());
    
  }
  
  public static fr.istic.shared.Person instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new fr.istic.shared.Person();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, fr.istic.shared.Person instance) throws SerializationException {
    streamWriter.writeString((String) com.google.gwt.core.client.impl.WeakMapping.get(instance, "server-enhanced-data-1"));
    streamWriter.writeObject(getEd(instance));
    streamWriter.writeObject(getFriends(instance));
    streamWriter.writeLong(getId(instance));
    streamWriter.writeObject(getMaison(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeString(getPrenom(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return fr.istic.shared.Person_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    fr.istic.shared.Person_FieldSerializer.deserialize(reader, (fr.istic.shared.Person)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    fr.istic.shared.Person_FieldSerializer.serialize(writer, (fr.istic.shared.Person)object);
  }
  
}
