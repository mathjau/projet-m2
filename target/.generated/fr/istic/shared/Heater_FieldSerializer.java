package fr.istic.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Heater_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getConsommation(fr.istic.shared.Heater instance) /*-{
    return instance.@fr.istic.shared.Heater::consommation;
  }-*/;
  
  private static native void setConsommation(fr.istic.shared.Heater instance, java.lang.String value) 
  /*-{
    instance.@fr.istic.shared.Heater::consommation = value;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native long getId(fr.istic.shared.Heater instance) /*-{
    return instance.@fr.istic.shared.Heater::id;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native void setId(fr.istic.shared.Heater instance, long value) 
  /*-{
    instance.@fr.istic.shared.Heater::id = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, fr.istic.shared.Heater instance) throws SerializationException {
    com.google.gwt.core.client.impl.WeakMapping.set(instance, "server-enhanced-data-1", streamReader.readString());
    setConsommation(instance, streamReader.readString());
    setId(instance, streamReader.readLong());
    
  }
  
  public static fr.istic.shared.Heater instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new fr.istic.shared.Heater();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, fr.istic.shared.Heater instance) throws SerializationException {
    streamWriter.writeString((String) com.google.gwt.core.client.impl.WeakMapping.get(instance, "server-enhanced-data-1"));
    streamWriter.writeString(getConsommation(instance));
    streamWriter.writeLong(getId(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return fr.istic.shared.Heater_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    fr.istic.shared.Heater_FieldSerializer.deserialize(reader, (fr.istic.shared.Heater)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    fr.istic.shared.Heater_FieldSerializer.serialize(writer, (fr.istic.shared.Heater)object);
  }
  
}
