package fr.istic.shared;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Home_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.List getConsommations(fr.istic.shared.Home instance) /*-{
    return instance.@fr.istic.shared.Home::consommations;
  }-*/;
  
  private static native void setConsommations(fr.istic.shared.Home instance, java.util.List value) 
  /*-{
    instance.@fr.istic.shared.Home::consommations = value;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native long getId(fr.istic.shared.Home instance) /*-{
    return instance.@fr.istic.shared.Home::id;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native void setId(fr.istic.shared.Home instance, long value) 
  /*-{
    instance.@fr.istic.shared.Home::id = value;
  }-*/;
  
  private static native fr.istic.shared.Person getPersonne(fr.istic.shared.Home instance) /*-{
    return instance.@fr.istic.shared.Home::personne;
  }-*/;
  
  private static native void setPersonne(fr.istic.shared.Home instance, fr.istic.shared.Person value) 
  /*-{
    instance.@fr.istic.shared.Home::personne = value;
  }-*/;
  
  private static native java.lang.String getRue(fr.istic.shared.Home instance) /*-{
    return instance.@fr.istic.shared.Home::rue;
  }-*/;
  
  private static native void setRue(fr.istic.shared.Home instance, java.lang.String value) 
  /*-{
    instance.@fr.istic.shared.Home::rue = value;
  }-*/;
  
  private static native java.lang.String getVille(fr.istic.shared.Home instance) /*-{
    return instance.@fr.istic.shared.Home::ville;
  }-*/;
  
  private static native void setVille(fr.istic.shared.Home instance, java.lang.String value) 
  /*-{
    instance.@fr.istic.shared.Home::ville = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, fr.istic.shared.Home instance) throws SerializationException {
    com.google.gwt.core.client.impl.WeakMapping.set(instance, "server-enhanced-data-1", streamReader.readString());
    setConsommations(instance, (java.util.List) streamReader.readObject());
    setId(instance, streamReader.readLong());
    setPersonne(instance, (fr.istic.shared.Person) streamReader.readObject());
    setRue(instance, streamReader.readString());
    setVille(instance, streamReader.readString());
    
  }
  
  public static fr.istic.shared.Home instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new fr.istic.shared.Home();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, fr.istic.shared.Home instance) throws SerializationException {
    streamWriter.writeString((String) com.google.gwt.core.client.impl.WeakMapping.get(instance, "server-enhanced-data-1"));
    streamWriter.writeObject(getConsommations(instance));
    streamWriter.writeLong(getId(instance));
    streamWriter.writeObject(getPersonne(instance));
    streamWriter.writeString(getRue(instance));
    streamWriter.writeString(getVille(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return fr.istic.shared.Home_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    fr.istic.shared.Home_FieldSerializer.deserialize(reader, (fr.istic.shared.Home)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    fr.istic.shared.Home_FieldSerializer.serialize(writer, (fr.istic.shared.Home)object);
  }
  
}
